<?php
    session_start();

    define("true_answer", [
        "answer_1" => "c",
        "answer_2" => "a",
        "answer_3" => "c",
        "answer_4" => "b",
        "answer_5" => "a",
        "answer_6" => "d",
        "answer_7" => "c",
        "answer_8" => "d",
        "answer_9" => "c",
        "answer_10" => "b",
    ]);

    $questions = [
        "Tại sao lá cây lại màu xanh?" => ["Trời cho", "Màu này đẹp", "Chất diệp lục", "Lá cây màu đỏ mà"],
        "Hoa đào nở vào mùa?" => ["Mùa xuân", "Mùa hè", "Mùa thu", "Mùa đông"],
        "Quả táo có vị gì?" => ["Cay", "Đắng", "Ngọt", "Bùi"],
        "Cây cho ra củ là?" => ["Cây mít", "Khoai lang", "Cây táo", "Hoa hồng"],
        "Con thỏ thích ăn gì?" => ["Cà rốt", "Cà tím", "Chuối", "Gạo"],
        "Con nào sau đây có bốn chân?" => ["Gà", "Rắn", "Vịt", "Mèo"],
        "Trái đất hình gì?" => ["bầu dục", "hộp chữ nhật", "cầu", "tam giác"],
        "Con gì biết phát sáng?" => ["Con ruồi", "Con mèo", "Con chó", "Con đom đóm"],
        "Fan Sơn tùng gọi là gì?" => ["Đóm", "Army", "Sky", "Gia đình Villa"],
        "Đạt villa gọi fan của mình là gì?" => ["Đóm", "Gia đình Villa", "Sky", "Army"]
    ];
    $answer_labels = ["a", "b", "c", "d"];

    $count = 0;
    $empty = 0;

    foreach (true_answer as $key => $answer) {
        if ($_SESSION[$key] == 'e') {
            $empty++;
        }
        if ($answer == $_SESSION[$key])
            $count++;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/result.css">
</head>
<body>
    <div class="wrapper">
        <div class="total">
            <?php
            if ($empty != 0) {
                echo "Bạn làm đúng " . $count . " câu " . "(" . $empty . " câu bị bỏ trống)" ."</br>";
            }
            else {
                echo "Bạn làm đúng " . $count . " câu " ."</br>";
            }
            if ($count < 4) {
                echo "Bạn quá kém, cần ôn tập thêm";
            }
            elseif ($count < 7) {
                echo "Cũng bình thường";
            }
            else {
                echo "Sắp sửa làm được trợ giảng lớp PHP";
            }
            ?>

        </div>

        <div class="true-answer">
            <?php
            $count = 1;
            foreach ($questions as $question => $answers) {
                $correct_answer = true_answer['answer_' . $count];
                $user_answer = $_SESSION['answer_' . $count];
                if ($user_answer == $correct_answer) {
                    $user_answer == "f";
                }
            ?>
                <div class="question-box">
                    <div class="empty-label"><?php echo ($user_answer == 'e' ? ' Bỏ trống' : '') ?></div>
                    <div class="question-filed"><?php echo "Câu " . $count . ": " . $question ?></div>
                    <?php
                    for ($i = 0; $i < sizeof($answers); $i++) {
                        $label = '';
                        if ($answer_labels[$i] == $correct_answer) {
                            $label = 'correct-answer';
                        }
                        else if (($answer_labels[$i] == $user_answer)) {
                            $label = 'wrong-answer';
                        }
                    ?>
                        <div class="answer-field">
                            <span class="dot <?php echo $label; ?>"></span>
                            <label class="answer-label <?php echo $label; ?>"><?php echo $answers[$i] ?></label>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            <?php
            $count++;
            }
            ?>
        </div>
    </div>
</body>
</html>